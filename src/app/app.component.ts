import { formatCurrency } from '@angular/common';
import { not } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, IterableDiffers, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { myNoteDatabase, MyNotes, myNoteType } from './shared/notes.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
// export class 


export class AppComponent {
title = 'Список заметок';
notes: MyNotes[]=myNoteDatabase;
copyNote!: {id: number;name: string;noteText: string; time: string;readonly:boolean;};
// readonly_!:boolean ;
// workers: MyWorker[]=MyWorkerDatabase;
// liveTime !: string;
ngOnInit(): void {
  // setInterval(() => { this.liveTime = moment().format('DD.MM.YYYY HH:mm:ss'); }, 1000)

}

  onAddNotes(note: MyNotes){
    let id = this.notes.length>0 ? this.notes[this.notes.length-1].id+1:0;
    note.id = id;
  
    this.notes.push(note);
  }
  
  onDeleteNotes(id:number){
    let index = this.notes.findIndex((note) =>
    note.id === id)
    // console.log("hello" + index);
   if (index !== -1) { 
  
     this.notes.splice(index, 1) }

  }
showNotes=true;
  onEditNotes(note: MyNotes){
   
    note.readonly=false;
    // this.readonly_=false;
    this.copyNote = note;
    this.showNotes=false;
    // this.onDeleteNotes(note.id);
  }


  onSaveNoteEdit(note: MyNotes) {
    // this.readonly_='readonly';
 
    // note.readonly=true;
  }
  //let newNote: { id: number, nameL: string, text: string, noteTime: string };
   
   
   onCancelNoteEdit(note: MyNotes) {
    // this.readonly_='readonly';
    
    // note.readonly=true;
  }








//   notes_:MyNotes[] = myNoteDatabase;
//  noteTypes_ = myNoteType;


  

//   noteArticle!: string;
//   noteBody!: string;
//   
  
//   deleteNote(index: number) {
//     this.testArr.splice(index, 1)
//   }
//   fillNotePad() {
//     if (this.noteArticle.trim() != "" && this.noteBody.trim() != "") {
//       //console.log("hw");
//       let newNote: { id: number, nameL: string, text: string, noteTime: string };
//       newNote = { id: 1, nameL: this.noteArticle, text: this.noteBody, noteTime: this.liveTime };
//       this.testArr.push(newNote);
//     }
    
//     this.noteArticle = "";
//     this.noteBody = "";

//   }
//   testArr: { id: number, nameL: string, text: string, noteTime: string }[] = [];

}

