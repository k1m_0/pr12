export interface MyNotes {
    id: number;
    name: string;
    noteText: string;
    time: string;
    // type:number;
    readonly:boolean;
    anyChang:boolean;
}
export enum myNoteType {
    a,
    b,
    c
}
export let myNoteDatabase: MyNotes[] = [{
    id: 1,
    name: 'Первая заметка',
    noteText: 'da-da-ya',
    time: (new Date).toLocaleString('ru'),
    readonly:true,
    anyChang:false
},
{
    id: 2,
    name: 'Вторая заметка',
    noteText: 'da-da-Ty',
    time: (new Date).toLocaleString('ru'),
    readonly:true,
    anyChang:false
}
]