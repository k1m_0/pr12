import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesCompComponent } from './notes-comp.component';

describe('NotesCompComponent', () => {
  let component: NotesCompComponent;
  let fixture: ComponentFixture<NotesCompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotesCompComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
