import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { myNoteDatabase, MyNotes } from '../shared/notes.model';

@Component({
  selector: 'app-notes-comp',
  templateUrl: './notes-comp.component.html',
  styleUrls: ['./notes-comp.component.css']
})
export class NotesCompComponent implements OnInit {
  changeName!:string
   changeText!:string
  //changeText= 'sadasdas';
  @Input() title!: string;
  @Input() notes: MyNotes[] = myNoteDatabase;;
  @Output() deleteNotes = new EventEmitter<number>();

  @Output() editNotes = new EventEmitter<MyNotes>();
  
  // @Output() saveNoteEdit = new EventEmitter<MyNotes>();
  // @Output() cancelNoteEdit = new EventEmitter<MyNotes>();
 
readonly_=true
  constructor() { }
  ngOnInit(): void {
  }


  onDeleteNotes1(id: number) {
    console.log(id);
    this.deleteNotes.emit(id);
  }
getROFlag(flag:boolean){
if(flag==true){

  return 'readonly'
}
else
return ''

  }
  changeNameNote():string{
    console.log('asdasdasdas')
    return 'name'
  }
  onEditNotes1(note: MyNotes) {
    for(let i of this.notes){
      i.anyChang= true;
     }
    this.changeName = note.name;
    this.changeText = note.noteText;
    
     this.editNotes.emit(note);
    
  }
  onSaveNoteEdit(note: MyNotes) {
    for(let i of this.notes){
      i.anyChang= false;
     }
    //this.editNotes.emit(note);
    note.name  = this.changeName ;
    note.noteText = this.changeText;
    console.log(note.name + note.noteText)
    note.readonly = true;
  }
  onCancelNoteEdit(note: MyNotes) {
    for(let i of this.notes){
      i.anyChang= false;
     }
     note.readonly = true;
    // this.editNotes.emit(note);
    
  }

  


}
