import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NotesCompComponent } from './notes-comp/notes-comp.component';
import { AddNoteComponent } from './ui/add-note/add-note.component';

@NgModule({
  declarations: [
    AppComponent,
    NotesCompComponent,
    AddNoteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
