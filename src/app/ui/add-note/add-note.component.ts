import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { myNoteDatabase, MyNotes } from 'src/app/shared/notes.model';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent implements OnInit {

  constructor() { }


  @Output() addNotes = new EventEmitter<MyNotes>();
 
  @Input() notes: MyNotes[] = myNoteDatabase;;
  // @Output() editNotes = new EventEmitter<number>();
  name!:string;
  noteText!:string;
  time = (new Date).toLocaleString('ru');

  ngOnInit(): void {
  }

  onAddNotes(){
    if(this.name.trim()=='' && this.noteText.trim()==''){ return}
    let note: MyNotes = {
      id:1,
      name: this.name,
      noteText: this.noteText,
      time: this.time,
      readonly: true,
      anyChang:false
    }
    this.name='' ;
     this.noteText='';
   this.addNotes.emit(note);

  }

  
}
